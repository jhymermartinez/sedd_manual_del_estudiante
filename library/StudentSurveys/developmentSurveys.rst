.. _developmentSurveys-title:

*************************************************
Sistema de Evaluación de Desempeño Docente (SEDD)
*************************************************

.. _developmentSurveys-system_access:

Acceso al Sistema
=================
Para empezar la encuesta ingresamos a la dirección `evaluaciondocente.unl.edu.ec <http:// http://evaluaciondocente.unl.edu.ec/>`_, seguidamente se presenta la pantalla de bienvenida al Sistema:

.. _developmentSurveys-img-developmentSurveys_index:

.. figure:: ../../_static/studentSurveys/developmentSurveys_index.png 
    :align: center
    :alt: Página de bienvenida
    :figclass: align-center

    **Página de Bienvenida**


El siguiente paso es hacer **clic** en el enlace **EVALUACIÓN DESEMPEÑO DOCENTE**:

.. _developmentSurveys-img-developmentSurveys_index_link:

.. figure:: ../../_static/studentSurveys/developmentSurveys_index_link.png 
    :align: center
    :alt: Enlace a la Encuesta
    :figclass: align-center

    **Enlace a la Encuesta**


Al ingresar a la página de Evaluación de Desempeño de los Docentes, el usuario se debe registrar con su nombre de usuario que será su número de cédula y su contraseña, la misma que utiliza para entrar al SGA, en la casilla correspondiente como observamos a continuación:

.. _developmentSurveys-img-developmentSurveys_login:

.. figure:: ../../_static/studentSurveys/developmentSurveys_login.png 
    :align: center
    :alt: Ingreso al Sistema
    :figclass: align-center

    **Ingreso al Sistema**


.. _developmentSurveys-development_surveys:

Desarrollo de la Encuesta Paso a Paso
=====================================


Una vez que el usuario se haya registrado correctamente, se ingresará directamente a su página personal. En esta pantalla aparecerá:

	• **Nombre del usuario.-** En la esquina superior a la derecha podrá visualizar su nombre.
	• **Carreras del estudiante.-** Se le presentara la lista de las carreras y cursos a los que esta inscrito.


.. _developmentSurveys-img-developmentSurveys_main:

.. figure:: ../../_static/studentSurveys/developmentSurveys_main.png 
    :align: center
    :alt: Carreras y cursos del usuario
    :figclass: align-center

    **Carreras y cursos del usuario**


Al seleccionar una carrera el sistema presenta todas las asignaturas pertenecientes a esta, con sus respectivos docentes. Aquí usted debe seleccionar el docente a evaluar, como podrá observar los docentes que están marcados con un visto son docentes ya evaluados:

.. _developmentSurveys-img-developmentSurveys_teaching:

.. figure:: ../../_static/studentSurveys/developmentSurveys_teaching.png 
    :align: center
    :alt: Docentes en proceso de evaluación
    :figclass: align-center

    **Docentes en proceso de evaluación**



Al seleccionar el docente, el sistema presenta la encuesta para evaluar a dicho docente pero antes un mensaje que indica que todas las preguntas son obligatorias incluidas las sugerencias:

.. _developmentSurveys-img-developmentSurveys_warning:

.. figure:: ../../_static/studentSurveys/developmentSurveys_warning.png 
    :align: center
    :alt: Mensaje inicial
    :figclass: align-center

    **Mensaje inicial**



A continuación se presenta la estructura inicial de la encuesta:

.. _developmentSurveys-img-developmentSurveys_complete:

.. figure:: ../../_static/studentSurveys/developmentSurveys_complete.png 
    :align: center
    :alt: Estructura de la Encuesta
    :figclass: align-center

    **Estructura de la Encuesta**


Una vez terminada, tenemos la opción de guardar o cancelar la encuesta:

.. _developmentSurveys-img-developmentSurveys_save_cancel_option:

.. figure:: ../../_static/studentSurveys/developmentSurveys_save_cancel_option.png 
    :align: center
    :alt: Guardar o Cancelar Encuesta
    :figclass: align-center

    **Guardar o Cancelar Encuesta**


Una vez seleccionada la opción de guardar, se presenta un mensaje de confirmación y el enlace para continuar hacia la página personal: (ver imagen :ref:`developmentSurveys-img-developmentSurveys_main`)

.. _developmentSurveys-img-developmentSurveys_message_complete:

.. figure:: ../../_static/studentSurveys/developmentSurveys_message_complete.png 
    :align: center
    :alt: Mensaje de Confirmación
    :figclass: align-center

    **Mensaje de Confirmación**


Si observamos con atención notamos que el docente evaluado aparece bloqueado lo que a su vez indica que ya ha sido evaluado correctamente:

.. _developmentSurveys-img-developmentSurveys_main_final:

.. figure:: ../../_static/studentSurveys/developmentSurveys_main_final.png 
    :align: center
    :alt: Docente Evaluado
    :figclass: align-center

    **Docente Evaluado**


Para salir del Sistema basta con hacer clic en la opción **Salir** como se aprecia a continuación:

.. _developmentSurveys-img-developmentSurveys_exit:

.. figure:: ../../_static/studentSurveys/developmentSurveys_exit.png 
    :align: center
    :alt: Salir del Sistema
    :figclass: align-center

    **Salida del Sistema**